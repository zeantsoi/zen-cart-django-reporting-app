from copy import deepcopy
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from belladreamhair.zen_cart import models


class ZenOrderBaseInline(admin.TabularInline):
	"""Base class to subclass for any TabularInline objects.
	Sets the default permissions."""
	def has_add_permission(self, request, obj=None):
		return False	
	def has_delete_permission(self, request, obj=None):
		return False


class ZenOrderProductAttributeInline(ZenOrderBaseInline):
	"""Renders attributes on a product as TabularInline.
	Subclasses ZenOrderBaseInline.
	"""
	model = models.ZenOrderProductAttribute
	readonly_fields = ('id', 'products_options', 'products_options_values', 'options_values_price',)

	def link(self, obj):
		return format_html("<a href='/admin/zen_cart/zenorderproductattribute/%s'>%s</a>" % (obj.id, obj.id))
	link.allow_tag = True


class ZenOrderProductInline(admin.TabularInline):
	"""Renders orders as TabularInline.
	Subclasses ZenOrderBaseInline.
	"""
	model = models.ZenOrderProduct
	readonly_fields = ('link', 'products_name', 'final_price', 'products_quantity',)

	def link(self, obj):
		return format_html("<a href='/admin/zen_cart/zenorderproduct/%s'>%s</a>" % (obj.id, obj.id))
	link.allow_tag = True


class ZenOrderProductAdmin(admin.ModelAdmin):
	"""Renders order products to a ModelAdmin.
	Displays ZenOrderProductAttributeInline as an inline.
	"""
	inlines = [ZenOrderProductAttributeInline]
	list_display = ('id', 'products_name', 'final_price', 'products_quantity',)
	readonly_fields = ('zenorder', 'id', 'products_name', 'final_price', 'products_quantity',)


class ZenOrderAdmin(admin.ModelAdmin):
	"""Renders order to a ModelAdmin.
	Displays ZenOrderProductInline as an inline.
	"""
	search_fields = ('customers_name', 'customers_email_address',)
	inlines = [ZenOrderProductInline]
	list_display = ('id', 'customers_name', 'customers_city', 'customers_state', 'customers_email_address', 'date_purchased', 'orders_status', 'order_total', 'order_tax',)
	list_filter = ('customers_name', 'date_purchased', 'order_total',)

	readonly_fields = ('customer', 'customers_name', 'customers_company', 'customers_street_address', 'customers_suburb', 'customers_city', 'customers_postcode', 'customers_state', 'customers_country', 'customers_telephone', 'customers_email_address', 'delivery_name', 'delivery_company', 'delivery_street_address', 'delivery_suburb', 'delivery_city', 'delivery_postcode', 'delivery_state', 'delivery_country', 'billing_name', 'billing_company', 'billing_street_address', 'billing_suburb', 'billing_city', 'billing_postcode', 'billing_state', 'billing_country', 'payment_method', 'payment_module_code', 'shipping_method', 'shipping_module_code', 'date_purchased', 'orders_status', 'order_total', 'order_tax',)


admin.site.register(models.ZenOrder, ZenOrderAdmin)
admin.site.register(models.ZenOrderProduct, ZenOrderProductAdmin)