from django.contrib.auth.models import User
from django.db import models
from mezzanine.core.models import Displayable


class ZenOrder(models.Model):
	"""Mocks a model for an order.
	Data comes from ZenCart, but is not originally formatted for Cartridge.
	Data is converted to Cartridge prior to DB import
	"""
	customer = models.ForeignKey(User)
	customers_name = models.CharField(max_length=255,)
	customers_company = models.CharField(max_length=255,)
	customers_street_address = models.CharField(max_length=255,)
	customers_suburb = models.CharField(max_length=255,)
	customers_city = models.CharField(max_length=255,)
	customers_postcode = models.CharField(max_length=255,)
	customers_state = models.CharField(max_length=255,)
	customers_country = models.CharField(max_length=255,)
	customers_telephone = models.CharField(max_length=255,)
	customers_email_address = models.CharField(max_length=255,)
	delivery_name = models.CharField(max_length=255,)
	delivery_company = models.CharField(max_length=255,)
	delivery_street_address = models.CharField(max_length=255,)
	delivery_suburb = models.CharField(max_length=255,)
	delivery_city = models.CharField(max_length=255,)
	delivery_postcode = models.CharField(max_length=255,)
	delivery_state = models.CharField(max_length=255,)
	delivery_country = models.CharField(max_length=255,)
	billing_name = models.CharField(max_length=255,)
	billing_company = models.CharField(max_length=255,)
	billing_street_address = models.CharField(max_length=255,)
	billing_suburb = models.CharField(max_length=255,)
	billing_city = models.CharField(max_length=255,)
	billing_postcode = models.CharField(max_length=255,)
	billing_state = models.CharField(max_length=255,)
	billing_country = models.CharField(max_length=255,)
	payment_method = models.CharField(max_length=255,)
	payment_module_code = models.CharField(max_length=255,)
	shipping_method = models.CharField(max_length=255,)
	shipping_module_code = models.CharField(max_length=255,)
	date_purchased = models.DateTimeField()
	orders_status = models.IntegerField(max_length=5,)
	order_total = models.DecimalField(max_digits=14, decimal_places=2)
	order_tax = models.DecimalField(max_digits=14, decimal_places=2)


class ZenOrderProduct(models.Model):
	"""Mocks a model for a product within an order."""
	zenorder = models.ForeignKey(ZenOrder)
	products_name = models.CharField(max_length=255,)
	final_price = models.DecimalField(max_digits=15, decimal_places=4)
	products_quantity = models.FloatField()


class ZenOrderProductAttribute(models.Model):
	"""Mocks an attribute on a product within an order."""
	zenorder = models.ForeignKey(ZenOrder)
	zenorderproduct = models.ForeignKey(ZenOrderProduct)
	products_options = models.CharField(max_length=255,)
	products_options_values = models.CharField(max_length=255,)
	options_values_price = models.DecimalField(max_digits=15, decimal_places=4)